# `> Cookbook`

Ce cookbook fournit des aides et astuces pour vous aider à configurer
et personnaliser votre environement `powerline.bash`.

## Choisir sa police

Les chevrons façon Powerline requièrent une police adaptée
[PowerLineSymbols](https://github.com/powerline/powerline/tree/develop/font) ou
[PowerlineExtraSymbols](https://github.com/ryanoasis/powerline-extra-symbols).
Vous pouvez soit configurer fontconfig pour utiliser la police Powerline, soit
utiliser une police combinée comme
[NerdFont](https://github.com/ryanoasis/nerd-fonts/).

Le projet Powerlevel9k a une [page wiki fournie sur les problèmes
d'affichage](https://github.com/Powerlevel9k/powerlevel9k/wiki/Troubleshooting)
des caractères Powerline.

Le site [s9w](http://s9w.io/font_compare/) compare visuellement les
polices pour vous aider à choisir celle qui vous convient.

Installer la police choisie avec votre gestionnaire de paquet favori
ou manuellement en copiant le fichier de la police.

## Personnaliser ses icônes

Si votre police n'inclut pas d'icônes adaptées vous pouvez utiliser
les *icônes* optionnelles avec
[icons-in-terminal](https://github.com/sebastiencs/icons-in-terminal/). 

Si vos icônes ne s'affichent pas ou si vous souhaitez personnaliser
vos icônes, regardez le code du caractère choisi
via [Gucharmap](https://en.wikipedia.org/wiki/GNOME_Character_Map) ou
`xfd -fa <nom de la police>` (sélectionner le caractère de votre choix
et voir en haut le champ `character` suivi du code héxadécimal) puis personnalisez
les variables d'environnement `POWERLINE_*_ICON`. Exemple:
```
xfd -fa "SauceCodePro Nerd Font:style=Semibold:size=10"
--> point d'exclamation avec un warning a le code Oxf071
    donc POWERLINE_FAIL_ICON=$'\uf071 '
```
Vous pouvez ajouter ces surcharges de `POWERLINE_*_ICON` dans votre
`.bashrc`. Vous pouvez aussi modifier `powerline.bash` en ajoutant une
nouvelle autoconfiguration (cf la procédure `__powerline_autoicons()`
dans le code) et proposer un patch à l'auteur de ce logiciel.

Si vous utilisez Nerd-fonts, il propose
une [Cheat Sheet](https://www.nerdfonts.com/cheat-sheet?set=nf-linux-)
pour trouver facilement un caractère par son nom, pour obtenir le
visuel et le code hexadécimal.

## Personnaliser les couleurs

Un grand tableau associatif `__powerline_colors` déclare la palette de couleur
et paramètre les couleurs des segments. Les couleurs de cette palette sont
définie en couleur de fond `48;5;XX`.

Pour surcharger les couleurs, définir le tableau associatif `POWERLINE_COLORS`
avant de sourcer `powerline.bash`.

``` bash
declare -A POWERLINE_COLORS
POWERLINE_COLORS=(
    [rouge]="48;5;148"  # Remplace le rouge par du vert...
)
. powerline.bash
```

### Personnaliser la couleur du segment `hostname`

Bien que la couleur du segment `hostname` est calculée fonction du contenu de ce dernier, il est possible de passer outre ce fonctionnement en spécifiant ses couleurs de fond et premier plan favorites avant d'invoquer `powerline.bash`:

```bash
POWERLINE_HOSTNAME_BG="48;5;172"
POWERLINE_HOSTNAME_FG="38;5;015"
. powerline.bash
```

Il est également possible d'utiliser les codes couleur de la palette :

```bash
POWERLINE_HOSTNAME_BG="blanc"
POWERLINE_HOSTNAME_FG="bleu-canard"
. powerline.bash
```


## Gnome

Un exemple d'installation manuelle avec Gnome:
- Téléchargement des .ttf originaux (icons-in-terminal et JetBrains Mono)
- Patch minimal avec nerd-font : juste powerline extra symbols.
- Installation des polices avec gnome-font-viewer.
- Configuration dans gnome-tweak-tools
- Configuration de fontconfig. (exemple: [10-bersace.conf](https://gitlab.com/bersace/dotfiles/-/blob/master/files/.config/fontconfig/conf.d/10-bersace.conf))

## Xterm

La configuration d'une police vectorielle pour xterm peut être un peu
complexe, donc voici les grandes lignes.
1. Trouver le *nom exact* de la police
   avec [Gucharmap](https://en.wikipedia.org/wiki/GNOME_Character_Map)
   ou avec `fc-list` de fontconfig. Exemple: 
``` console
$ fc-list : family style file | grep SauceCodePro
...
/usr/share/fonts/nerd-fonts-complete/TTF/Sauce Code Pro Semibold Nerd Font Complete.ttf: SauceCodePro Nerd Font:style=Semibold,Regular
...
-> une police avec deux styles
   "SauceCodePro Nerd Font:style=Semibold" et "SauceCodePro Nerd Font:style=Regular"
```
2. [Facultatif] Tester la police avec xterm: `xterm -fa "SauceCodePro Nerd Font:style=Semibold:size=10"`
3. Configurer cette police dans .Xresources en ajoutant/remplaçant la
   ligne `XTerm*faceName: SauceCodePro Nerd Font:style=Semibold:size=10` 
4. Charger ces ressources (`xrdb -merge ~/.Xresources`) et lancer une
   nouvelle instance de xterm pour vérifier
