#!/bin/bash -eu
#
# Affiche la palette des 256 couleurs RGB
#


afficher_couleur() {
	local couleur="$1"
        printf -v texte "%7s" ${2-$couleur}
        echo -en "\\e[48;5;${couleur}m ${texte} "
}

for couleur in {0..15} {232..255} ; do
	afficher_couleur $couleur
        if [ "$(( (1 + couleur) % 8))" -eq 0 ] ; then
                echo -e "\\e[0m"
        fi 
done

for rv in {0..5}{0..5} ; do
	r=${rv:0:1}
	v=${rv:1:1}
	for b in {0..5} ; do
		couleur=$(( 16 + $r * 36 + $v * 6 + $b))
		afficher_couleur $couleur "$r$v$b=$couleur"
	done
	echo -e "\\e[0m"
done
